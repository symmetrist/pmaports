# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=polkit-kde-agent
pkgver=5.15.2
pkgrel=0
pkgdesc="Daemon providing a polkit authentication UI for KDE"
arch="all"
url="https://www.kde.org/workspaces/plasmadesktop/"
license="GPL-2.0"
depends_dev="qt5-qtbase-dev kiconthemes-dev kdbusaddons-dev kcrash-dev polkit-qt-dev ki18n-dev kwindowsystem-dev kwidgetsaddons-dev kcoreaddons-dev"
makedepends="$depends_dev extra-cmake-modules"
source="https://download.kde.org/stable/plasma/$pkgver/$pkgname-1-$pkgver.tar.xz"
subpackages="$pkgname-lang"
builddir="$srcdir/$pkgname-1-$pkgver"

build() {
	cmake \
		-DCMAKE_BUILD_TYPE=Release \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DCMAKE_INSTALL_LIBEXECDIR=lib
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="a877bdf451e551a70cbb1d5f40ade3a128f10b4cbd55443985dcb5e44ef7b245a9de2c990543aba91f28f15af0f5b3801e3c655084a803bfb7f3c5427ad08589  polkit-kde-agent-1-5.15.2.tar.xz"
