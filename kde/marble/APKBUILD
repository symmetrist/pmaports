# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=marble
pkgver=18.12.2
pkgrel=0
pkgdesc="A Virtual Globe and World Atlas that you can use to learn more about Earth"
arch="all"
url='https://marble.kde.org'
license="iGFDL-1.2 BSD-3.0 GPL-3.0"
depends="krunner"
depends_dev="qt5-qtbase-dev qt5-qtsvg-dev qt5-qtdeclarative-dev qt5-qtlocation-dev qt5-qtwebkit-dev qt5-qtserialport-dev kcoreaddons-dev kwallet-dev knewstuff-dev kparts-dev plasma-framework-dev ki18n-dev kio-dev kcrash-dev krunner-dev shared-mime-info gpsd-dev"
makedepends="$depends_dev extra-cmake-modules"
source="https://download.kde.org/stable/applications/$pkgver/src/$pkgname-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
options="!check" # Broken

build() {
	cmake \
		-DCMAKE_BUILD_TYPE=Release \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DCMAKE_INSTALL_SYSCONFDIR=/etc \
		-DQT_PLUGINS_DIR=lib/qt/plugins \
		-DMOBILE=ON \
		-DBUILD_MARBLE_APPS=YES
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}
sha512sums="74dba5cfc7c5b9995445817667148417fdd36994ddf105484e2d13f4a5dff7654f98d84680ff26c5057a60d0d9cbfbe7dbc3e7d3413e081fb67f3ced2783286c  marble-18.12.2.tar.xz"
