# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kplotting
pkgver=5.55.0
pkgrel=0
pkgdesc="Lightweight plotting framework"
arch="all"
url="https://community.kde.org/Frameworks"
license="LGPL-2.1"
depends_dev="qt5-qtbase-dev"
makedepends="$depends_dev extra-cmake-modules doxygen qt5-qttools-dev"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/$pkgname-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc"
options="!check" # Fails due to requiring running X11

build() {
	cd "$builddir"
	cmake . \
		-DCMAKE_BUILD_TYPE=Release \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="4e7dcec142a3cf5855a8e14246f8878b077a819b5cb73407da3534a97a11336b902d6185dba256fe2df834e5aec686dedc7a3ce6a64ed6a73a75bd370954822a  kplotting-5.55.0.tar.xz"
